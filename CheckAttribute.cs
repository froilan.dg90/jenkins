using System;
using System.Runtime.CompilerServices;

namespace test
{
    [System.AttributeUsage(System.AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class MyAttribute : System.Attribute
    {
        // See the attribute guidelines at
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        readonly string positionalString;
        
        // This is a positional argument
        public MyAttribute([CallerMemberName]string positionalString = null)
        {
            this.positionalString = positionalString;
            System.Console.WriteLine(">>>>>" + positionalString);
        }
        
        public string PositionalString
        {
            get { return positionalString; }
        }
        
        // This is a named argument
        public int NamedInt { get; set; }
    }
}